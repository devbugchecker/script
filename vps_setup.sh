# Define
echo "Your email: "
read -e EMAIL
# Update
sudo apt-get update 
sudo apt-get upgrade -y 
sudo apt-get install software-properties-common python3-software-properties curl wget -y
# Git
sudo apt-get install git git-core -y 
# Zsh
sudo apt-get install zsh -y 
# Java
sudo add-apt-repository ppa:webupd8team/java -y 
sudo apt-get update 
sudo apt-get install oracle-java8-installer -y 
sudo apt-get install oracle-java8-set-default
# Composer
sudo apt-get update 
sudo apt-get install curl -y 
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
# Apache
sudo apt-get update 
sudo apt-get install apache2 apache2-utils php5 libapache2-mod-php5 -y
# Nginx
sudo apt-get update 
sudo apt-get install nginx -y
# Mysql
sudo apt-get install mysql-server-5.6 -y 
sudo mysql_install_db 
sudo mysql_secure_installation
# Php
sudo add-apt-repository ppa:ondrej/php -y 
sudo apt-get update 
sudo apt-get install php7.0 php7.0-fpm -y 
sudo apt-get install libapache2-mod-php7.0 php7.0-dev php7.0-mysql php7.0-cli php7.0-gd php7.0-json -y
# Nodejs
sudo apt-get update 
sudo apt-get install nodejs nodejs-legacy -y 
sudo apt-get install npm -y 
# Grunt
npm install -g grunt-cli
# Adminer
cd /var/www/ 
sudo chown `whoami`:`whoami` -R html 
cd html 
wget https://www.adminer.org/static/download/4.2.5/adminer-4.2.5.php 
mv adminer-4.2.5.php adminer.php 

# Ruby
cd ~
sudo apt-get update
sudo apt-get install curl -y
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -L https://get.rvm.io | bash -s stable
source /etc/profile.d/rvm.sh
rvm requirements
rvm install ruby
rvm use ruby --default
rvm rubygems current
gem update --system
gem install sass
gem install compass
gem install bundler
gem install jekyll
gem install github-pages

# Othe dev packages 
sudo apt-get install vim -y       
sudo apt-get install sshpass -y
sudo apt-get install htop -y
sudo apt-get install ranger -y
sudo apt-get install cmatrix -y
sudo apt-get install sl -y
sudo apt-get install nmap -y
sudo apt-get install unrar -y
sudo apt-get install tmux -y
sudo apt-get install xclip -y
sudo apt-get install cowsay -y
sudo apt-get install figlet -y

# Docker
sudo apt-get update 
wget -qO- https://get.docker.com/ | sh
sudo usermod -aG docker $(whoami)
sudo apt-get -y install python-pip
sudo pip install docker-compose

# Config
echo 'alias cls="clear"' >> ~/.bashrc
echo 'alias cls="clear"' >> ~/.zshrc
sudo sed -i -r "s/^enabled=.*/enabled=0/" /etc/default/apport
git config --global user.email "$EMAIL"
git config --global user.name "`whoami`"
sudo chown `whoami`:www-data -R /var/www
echo '[alias]
    rb = rebase
    me = merge
    co = checkout
    br = branch
    cm = commit
    amend = commit -a --amend
    rm = "!f() { git branch -D ${1-develop}; }; f"
    st = status
    cp = cherry-pick
    last = log -1 HEAD
    lg = log --oneline
    cl = clean -fxd
    grep = !git ls-files | grep -i
    gs = diff --staged' >> ~/.gitconfig

# Restart
sudo apt-get update
sudo apt-get upgrade
