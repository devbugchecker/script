# Install Ioncube on Ubuntu #
cd ~
wget http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz
tar xvfz ioncube_loaders_lin_x86-64.tar.gz
php -i | grep extension_dir
# get extension dir
PHP_VERSION=$(php -r "echo PHP_MAJOR_VERSION.'.'.PHP_MINOR_VERSION;")
sudo cp "ioncube/ioncube_loader_lin_${PHP_VERSION}.so" /usr/lib/php/20151012
sudo sed -i "2izend_extension = /usr/lib/php/20151012/ioncube_loader_lin_7.0.so\n" /etc/php/7.0/apache2/php.ini
sudo sed -i "2izend_extension = /usr/lib/php/20151012/ioncube_loader_lin_7.0.so\n" /etc/php/7.0/cli/php.ini
sudo sed -i "2izend_extension = /usr/lib/php/20151012/ioncube_loader_lin_7.0.so\n" /etc/php/7.0/fpm/php.ini
sudo service apache2 restart
# sudo service nginx restart
sudo service php7.0-fpm restart
# check extension is loaded
php -r "var_export(extension_loaded('ionCube Loader') ,true);"
php -v