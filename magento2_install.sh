# Define variable
ROOT_DIR='/var/www/test'
GIT_REMOTE='git@github.com:dhduc/magento2.git'
GIT_OPTIONS=''
VHOST='test.conf'
DOMAIN='test.local'

ENDC='\033[0m'
RED='\033[91m'
GREEN='\033[92m'
BLUE='\033[94m'

init() {
	echo $GREEN'Install and setup magento 2 project'$ENDC
	echo $BLUE'System prerequires: \nApache 2.x\nMySQL 5.6\nPHP 7.0\nNodejs 0.10.x'$ENDC
}

clone()
{
	echo $GREEN'Create and clone project to' $ROOT_DIR $ENDC
	if [ -d $ROOT_DIR ]; then
		rm -rf $ROOT_DIR
	fi
	git clone $GIT_OPTIONS $GIT_REMOTE $ROOT_DIR	
}

vhost()
{
	echo $GREEN'Create and enable vhost for' $DOMAIN $ENDC
	cd /etc/apache2/sites-available
	if [ -s $VHOST ]; then
		sudo rm $VHOST
	else
		sudo touch $VHOST
	fi	

	sudo -- sh -c -e "echo '
		<VirtualHost *:80>
		\n\tServerName $DOMAIN
		\n\tServerAdmin webmaster@localhost
		\n\tDocumentRoot $ROOT_DIR
		\n\tErrorLog ${APACHE_LOG_DIR}/error.log
		\n</VirtualHost>
		' > $VHOST"

	sudo sed -i "1i127.0.0.1 $DOMAIN\n" /etc/hosts

	sudo a2ensite $VHOST
}

setAcl() {
	echo $GREEN'Setup ownership and permissions for' $ROOT_DIR $ENDC
	cd $ROOT_DIR
	
	if [ -d 'var' ]; then
		sudo rm -rf var
	fi
		
	find . -type d -exec chmod 775 {} \; && find . -type f -exec chmod 664 {} \;
	HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
	sudo chown -R `whoami`:"$HTTPDUSER" .
	sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var pub/static pub/media app/etc
	sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var pub/static pub/media app/etc
	chmod u+x bin/magento
}

setup() {
	echo $GREEN'Install Magento 2'$ENDC
	php bin/magento setup:install \
	  --admin-firstname=First \
	  --admin-lastname=Last \
	  --admin-email='email@example.com' \
	  --admin-user=admin \
	  --admin-password='ADMIN_PASSWORD' \
	  --base-url='http://localhost' \
	  --backend-frontname=admin \
	  --db-host='DB_HOST' \
	  --db-name='DB_NAME' \
	  --db-user='DB_USER' \
	  --db-password='DB_PASSWORD' \
	  --language=en_US \
	  --currency=USD \
	  --timezone='America/Chicago' \
	  --admin-use-security-key=0 \
	  --session-save=files
	php bin/magento deploy:mode:set developer
}

setNode() {
	echo $GREEN'Setup Grunt & Nodejs dev'$ENDC
	if [ -s 'Gruntfile.js.sample' ]; then
		mv Gruntfile.js.sample Gruntfile.js
	fi
	npm install -g grunt-cli
	npm install
}

finish() {
	echo $GREEN'Restart Apache and PHP service'$ENDC
	sudo service php7.0-fpm restart
	sudo service apache2 restart
	echo $GREEN'Go to' $DOMAIN $ENDC
	google-chrome $DOMAIN
}

init
clone
vhost
setAcl
setup
setNode
finish