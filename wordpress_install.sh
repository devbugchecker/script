# Define variable
ROOT_DIR='/var/www/blog'
GIT_REMOTE='git@github.com:WordPress/WordPress.git'
GIT_OPTIONS=''
VHOST='blog.conf'
DOMAIN='mblog.local'
DBNAME='mblog'
DBUSER='root'
DBPASS='root'
DBHOST='localhost'

ENDC='\033[0m'
RED='\033[91m'
GREEN='\033[92m'
BLUE='\033[94m'

init() {
	echo $GREEN'Install and setup wordpress project at ' $(date + '%T') $ENDC
	echo $BLUE'System prerequires: \nApache 2.x\nMySQL 5.x\nPHP 5.x\nNodejs 0.10.x'$ENDC
}

clone()
{
	echo $GREEN'Create and clone project to' $ROOT_DIR $ENDC
	if [ -d $ROOT_DIR ]; then
		rm -rf $ROOT_DIR
	fi
	git clone $GIT_OPTIONS $GIT_REMOTE $ROOT_DIR
	cd $ROOT_DIR
	git config core.fileMode false
}

vhost()
{
	echo $GREEN'Create and enable vhost for' $DOMAIN $ENDC
	cd /etc/apache2/sites-available
	if [ -s $VHOST ]; then
		sudo rm $VHOST
	else
		sudo touch $VHOST
	fi	

	sudo -- sh -c -e "echo '
		<VirtualHost *:80>
		\n\tServerName $DOMAIN
		\n\tServerAdmin webmaster@localhost
		\n\tDocumentRoot $ROOT_DIR
		\n\tErrorLog ${APACHE_LOG_DIR}/error.log
		\n</VirtualHost>
		' > $VHOST"

	sudo sed -i "1i127.0.0.1 $DOMAIN\n" /etc/hosts

	sudo a2ensite $VHOST
}

setAcl() {
	echo $GREEN'Setup ownership and permissions for' $ROOT_DIR $ENDC
	cd $ROOT_DIR
		
	HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
	sudo chown -R `whoami`:"$HTTPDUSER" .
	find . -type d -exec chmod 775 {} \; && find . -type f -exec chmod 664 {} \;
}

setup() {
	echo $GREEN'Setup database' $ENDC
	echo $GREEN'Create database' $DBNAME $ENDC

	RESULT=`mysql -u$DBUSER -p$DBPASS -e "SHOW DATABASES" | grep -Fo $DBNAME`
	if [ "$RESULT" = "$DBNAME" ]; then
		mysql -u$DBUSER -p$DBPASS -e "DROP DATABASE $DBNAME;"
	fi
	
	mysql -u$DBUSER -p$DBPASS -e "CREATE DATABASE $DBNAME CHARACTER SET utf8 COLLATE utf8_general_ci;"

	cd $ROOT_DIR
	cp wp-config-sample.php wp-config.php
	
	if [ -s 'wp-config.php' ]; then
		perl -pi -e "s/database_name_here/$DBNAME/g" wp-config.php
		perl -pi -e "s/username_here/$DBUSER/g" wp-config.php
		perl -pi -e "s/password_here/$DBPASS/g" wp-config.php
		perl -pi -e "s/localhost/$DBHOST/g" wp-config.php
	fi
}

finish() {
	echo $GREEN'Restart Apache'$ENDC
	sudo service apache2 restart
	echo $GREEN'Complete install and setup wordpress project at' $(date +'%T') $ENDC
	echo $BLUE'Open browser then go to' $DOMAIN $ENDC
}

init
clone
vhost
setAcl
setup
finish
