#!/bin/sh

# Update
sudo apt-get update 
sudo apt-get upgrade -y 
sudo apt-get install software-properties-common
sudo apt-get install python3-software-properties

# Git
sudo apt-get install git git-core -y 
sudo apt-get install gitk gitg

# Shell
sudo apt-get install zsh -y 
sudo apt-get install fish -y 

# Tool
sudo apt-get install curl -y
sudo apt-get install wget -y
sudo apt-get install sshpass -y
sudo apt-get install htop -y
sudo apt-get install ranger -y
sudo apt-get install cmatrix -y
sudo apt-get install sl -y
sudo apt-get install nmap -y
sudo apt-get install unrar -y
sudo apt-get install tmux -y
sudo apt-get install xclip -y
sudo apt-get install cowsay -y
sudo apt-get install figlet -y
sudo apt-get install jq -y
sudo apt-get install figlet -y
sudo apt-get install boxes -y
sudo apt-get install tig -y
sudo apt-get install unp -y
sudo apt-get install ncurses-term
sudo apt-get install screen
sudo apt-get install ccze
sudo apt-get install chkconfig -y
sudo apt-get install ibus ibus-unikey -y 

# Vim
sudo apt-get install vim -y       
curl -sL https://raw.githubusercontent.com/egalpin/apt-vim/master/install.sh | sh
apt-vim install -y https://github.com/scrooloose/nerdtree.git

# nnn
sudo add-apt-repository ppa:twodopeshaggy/jarun -y
sudo apt update
sudo apt install nnn -y

# Java
sudo add-apt-repository ppa:webupd8team/java -y 
sudo apt-get update 
sudo apt-get install oracle-java8-installer -y 
sudo apt-get install oracle-java8-set-default

# Apache
sudo apt-get install apache2 apache2-utils libapache2-mod-php5 libapache2-mod-php7.0 -y
echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/fqdn.conf
sudo a2enconf fqdn
sudo a2enmod rewrite

# Nginx
sudo apt-get install nginx -y
sudo service nginx stop
sudo update-rc.d nginx disable

# Mysql
sudo apt-get install mysql-server mysql-client
sudo mysql_install_db 
sudo mysql_secure_installation

# Php
sudo add-apt-repository ppa:ondrej/php -y 
sudo apt-get update 
sudo apt-get install composer
sudo apt-get install php7.0 php7.0-fpm -y 
sudo apt-get install php7.0-dev php7.0-mysql php7.0-cli php7.0-gd php7.0-json php7.0-mbstring  php7.0-dom php7.0-simplexml php7.0-xml -y
sudo apt-get install php7.0-mysql php7.0-mcrypt php7.0-curl php7.0-gd libcurl3 php7.0-intl php7.0-xsl php7.0-zip php7.0-bcmath php7.0-imagick -y

# Php Tool
wget https://files.magerun.net/n98-magerun2.phar
chmod +x ./n98-magerun2.phar
sudo mv ./n98-magerun2.phar /usr/local/bin/n98

# Phpunit
cd ~
wget https://phar.phpunit.de/phpunit.phar
chmod +x phpunit.phar
sudo mv phpunit.phar /usr/local/bin/phpunit
cd ~

# Phpcs
cd ~
curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
chmod +x phpcs.phar
sudo mv phpcs.phar /usr/local/bin/phpcs
cd ~

# Xdebug
cd ~
wget http://xdebug.org/files/xdebug-2.4.1.tgz
tar -xvzf xdebug-2.4.1.tgz
cd xdebug-2.4.1
phpize
./configure
make
sudo cp modules/xdebug.so /usr/lib/php/20151012
sudo -- sh -c -e "echo '
	\nzend_extension = /usr/lib/php/20151012/xdebug.so
	\nxdebug.idekey = \'PHPSTORM\'
	\nxdebug.remote_autoStart=0
	\nxdebug.remote_enable=1
	\nxdebug.remote_port=9000
	\nxdebug.remote_connect_back=1
	\nxdebug.remote_handler=dbgp
	' >> /etc/php/7.0/cli/php.ini"
cd ~

# Nodejs
sudo apt-get install nodejs -y 
sudo apt-get install npm -y
sudo ln -s "$(which nodejs)" /usr/bin/node
sudo npm install -g grunt-cli
sudo npm install -g bower
sudo npm install -g tldr
sudo npm install -g http-server

# Docker
curl -sSL https://get.docker.com/ | sh
sudo usermod -aG docker `whoami`

# Pip
sudo apt install python-pip
pip install --upgrade pip
sudo pip install docker-compose

# GUI
sudo apt-get install filezilla -y
sudo apt-get install vlc -y
sudo apt-get install mozilla-plugin-vlc -y
sudo apt-get install adobe-flashplugin -y

# Chrome
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - 
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list' -y 
sudo apt-get update 
sudo apt-get install google-chrome-stable -y

# Sublime
sudo add-apt-repository ppa:webupd8team/sublime-text-3 -y 
sudo apt-get update 
sudo apt-get install sublime-text-installer -y

# Skype
cd ~
wget https://repo.skype.com/latest/skypeforlinux-64.deb
sudo dpkg -i skypeforlinux-64.deb
rm skypeforlinux-64.deb
cd ~

# Terminator
sudo add-apt-repository ppa:gnome-terminator -y 
sudo apt-get update 
sudo apt-get install terminator -y

# PhpStorm
cd ~ 
wget http://download.jetbrains.com/webide/PhpStorm-2016.3.3.tar.gz -O phpstorm.tar.gz
tar -xvzf phpstorm.tar.gz
mv PhpStorm-163.13906.21 phpstorm
chmod u+x phpstorm/bin/phpstorm.sh
rm phpstorm.tar.gz
cd ~ 

# Navicat
cd ~ 
wget http://download3.navicat.com/download/navicat112_premium_en_x64.tar.gz 
tar -xzvf navicat112_premium_en_x64.tar.gz 
mv navicat112_premium_en_x64 navicat 
chmod u+x navicat/Start_navicat
rm navicat112_premium_en_x64.tar.gz
cd ~ 

# Adminer
cd /var/www/ 
sudo chown `whoami`:`whoami` -R html 
wget https://www.adminer.org/static/download/4.2.5/adminer-4.2.5.php
mkdir /var/www/html/adminer
mv adminer-4.2.5.php /var/www/html/adminer/index.php 
cd ~ 

# Unity Tweak Tool
sudo add-apt-repository ppa:freyja-dev/unity-tweak-tool-daily -y 
sudo apt-get update 
sudo apt-get install unity-tweak-tool -y

# Theme Paper
sudo add-apt-repository ppa:snwh/pulp -y 
sudo apt-get update  
sudo apt-get install paper-gtk-theme paper-icon-theme -y

# Icon Numix
sudo add-apt-repository ppa:numix/ppa -y 
sudo apt-get update 
sudo apt-get install numix-gtk-theme numix-icon-theme numix-icon-theme-circle -y

# Wine
sudo add-apt-repository ppa:ubuntu-wine/ppa -y 
sudo apt-get update 
sudo apt-get install wine1.7 -y

# PlayLinux
wget -q "http://deb.playonlinux.com/public.gpg" -O- | sudo apt-key add - 
sudo wget http://deb.playonlinux.com/playonlinux_trusty.list -O /etc/apt/sources.list.d/playonlinux.list -y 
sudo apt-get update 
sudo apt-get install playonlinux -y

# Webmin
echo 'deb http://download.webmin.com/download/repository sarge contrib' >> sudo nano /etc/apt/sources.list
wget http://www.webmin.com/jcameron-key.asc
sudo apt-key add jcameron-key.asc
sudo apt-get install webmin -y

# Chrome gnome shell
sudo add-apt-repository ppa:ne0sight/chrome-gnome-shell
sudo apt-get update
sudo apt-get install chrome-gnome-shell

# Virtualbox
sudo apt-get install dkms -y
wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc -O- | sudo apt-key add -
sudo sh -c 'echo "deb http://download.virtualbox.org/virtualbox/debian trusty contrib" >> /etc/apt/sources.list.d/virtualbox.list' -y
sudo apt-get update
sudo apt-get install virtualbox-5.0 -y

# Grub customizer
sudo add-apt-repository ppa:danielrichter2007/grub-customizer -y
sudo apt-get update
sudo apt-get install grub-customizer -y

# Bleachbit
sudo add-apt-repository ppa:n-muench/programs-ppa -y 
sudo apt-get update 
sudo apt-get install bleachbit -y

# Tweak
sudo add-apt-repository ppa:tualatrix/ppa -y
sudo apt-get update
sudo apt-get install ubuntu-tweak -y

# Pinta
sudo add-apt-repository ppa:pinta-maintainers/pinta-stable -y
sudo apt-get update
sudo apt-get install pinta -y

# Rbenv
sudo apt-get install autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev -y
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.zshrc
echo 'eval "$(rbenv init -)"' >> ~/.zshrc
source ~/.zshrc
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
rbenv install 2.3.1
rbenv global 2.3.1
ruby -v

# RVM
bash
sudo apt-get update
sudo apt-get install build-essential make curl
curl -L https://get.rvm.io | bash -s stable
source ~/.bash_profile
rvm install ruby-2.1.4
rvm use --default ruby-2.1.4$HOME/.rvm/rubies export PATH=$PATH:$HOME/.rvm/rubies/ruby-2.1.4/bin
zsh

# Gem packages
sudo apt-get install rubygems -y
gem install rubygems-update
sudo gem update
sudo gem install sass
sudo gem install compass
sudo gem install bundler
sudo gem install jekyll
sudo gem install github-pages

# Restart
ibus restart
sudo chsh --shell $(which fish)
sudo service apache2 restart
sudo apt-get update
sudo apt-get upgrade
